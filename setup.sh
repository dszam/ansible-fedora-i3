#!/bin/bash
# setup the machine

echo "install ansible"
sudo dnf -y install ansible

echo "installing all required roles"
ansible-galaxy install -r requirements.yml

echo "run playbook"
ansible-playbook -K main.yml